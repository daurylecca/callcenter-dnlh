package co.com.almundo.domain;

import java.time.LocalDateTime;
import java.util.Random;

import org.apache.log4j.Logger;

import co.com.almundo.utils.EmployeeStatus;

public class Call  implements Runnable{
	/** Logger constant. */
	private static final Logger logger = Logger.getLogger(Call.class);
	
	/** Duration constant*/
	public static final int MIN_DURATION = 5;
	public static final int MAX_DURATION = 10;
	
	private long id;
	private String description;
	private AbstractEmployee employee;

		
	public Call (long id, String description, AbstractEmployee employee){
		this.id = id;
		this.description = description;
		this.employee = employee;
		
	}

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	@Override
	public void run() {
		try {
			employee.handleCall(this);
			int callDuration = getDuration();
			logger.info("Call: " + id
					+ ", Client name: " + description
					+ ", Initial time: " + LocalDateTime.now()
					+ ", Duration: " + callDuration / 1000 + " Seconds");
			
			waitForSeconds(callDuration);
			
			employee.setStatus(EmployeeStatus.AVAILABLE.getStatus());
			
			logger.info("Finished Call: " + id + ", Final time: " + LocalDateTime.now());
		} catch (NullPointerException e) {
			logger.info("There are no free employees to answer the call. " + " Call ID: " + id); 
		}
		
	}
	
	public int getDuration() {
		return (new Random().nextInt(MAX_DURATION - MIN_DURATION + 1) + MIN_DURATION) * 1000 ;
	}
	
	public void waitForSeconds(int callDuration) {
		try {
			Thread.sleep(callDuration);
		}catch (InterruptedException ex){
			Thread.currentThread().interrupt();
			logger.error("ERROR " + ex);
		}
	}
}
