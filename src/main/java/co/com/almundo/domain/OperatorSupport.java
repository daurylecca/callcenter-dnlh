package co.com.almundo.domain;

import org.apache.log4j.Logger;

import co.com.almundo.utils.EmployeeStatus;

public class OperatorSupport extends AbstractEmployee{
	/** Logger constant. */
	private static final Logger logger = Logger.getLogger(OperatorSupport.class);
	

	@Override
	public void handleCall(Call call) {
		if (this.isFree()){   
			this.setStatus(EmployeeStatus.ONCALL.getStatus());
			logger.info("The call " + call.getId() + " was answered for employee " + this.getName());
			
			
		} else{
			successor.handleCall(call);
		}
	}

}
