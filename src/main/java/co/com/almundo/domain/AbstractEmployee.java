package co.com.almundo.domain;

import co.com.almundo.utils.EmployeeStatus;

public abstract class AbstractEmployee{
	
	
	protected AbstractEmployee successor;
	private String status;
	private String name;
	private String typeEmployee;
	
	public AbstractEmployee() {
		this.status = EmployeeStatus.AVAILABLE.getStatus();
	}
	
	
	public void setStatus(String status) {
	      this.status = status;
	}
	
	public String getStatus() {
	   return this.status;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setTypeEmployee(String typeEmployee) {
	      this.typeEmployee = typeEmployee;
	}
	
	public String getTypeEmployee() {
		return typeEmployee;
	}
	
	public void setSuccessor(AbstractEmployee successor) {
		this.successor = successor;
	}
	
	public boolean isFree() {
	      return status == EmployeeStatus.AVAILABLE.getStatus();
	}
	
	abstract void handleCall(Call call);

	
}
