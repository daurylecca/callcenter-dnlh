package co.com.almundo.utils;

public enum EmployeeStatus {
	ONCALL("ONCALL"),
    AVAILABLE ("AVAILABLE");
    
    private String status;
	
	EmployeeStatus(String status){
		
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
}
