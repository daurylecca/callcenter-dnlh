package co.com.almundo.utils;

public enum EmployeeType {
	OPERATOR(0, "OPERATOR"),
	SUPERVISOR(1, "SUPERVISOR"),
	DIRECTOR(2, "DIRECTOR");
	
	private int priority;
	private String type;
	
	EmployeeType(int priority, String type){
		this.priority = priority;
		this.type= type;
	}
	
	public int getPriority(){
		return priority;
	}

	public String getType() {
		return type;
	}
}
