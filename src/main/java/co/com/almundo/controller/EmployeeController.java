package co.com.almundo.controller;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import co.com.almundo.domain.AbstractEmployee;
import co.com.almundo.utils.EmployeeStatus;
import co.com.almundo.utils.EmployeeType;

public class EmployeeController {
	Map<String, Deque<AbstractEmployee>> employeesWorking;
	
	public EmployeeController() {
		
		employeesWorking = new HashMap<>();
		employeesWorking.put(EmployeeType.OPERATOR.getType(), new LinkedList<AbstractEmployee>());
		employeesWorking.put(EmployeeType.SUPERVISOR.getType(), new LinkedList<AbstractEmployee>());
		employeesWorking.put(EmployeeType.DIRECTOR.getType(), new LinkedList<AbstractEmployee>());
		
	}
	
	public AbstractEmployee getFreeEmployee() {
		AbstractEmployee employee = null;
		boolean flag = false;
		
		int priorityEmployee = 0;
		
		while (priorityEmployee < employeesWorking.size() && !flag) {
			int employeePriority = 0;
			String dirType = priorityEmployee == 2? EmployeeType.DIRECTOR.getType() : "";
			String supType = priorityEmployee == 1? EmployeeType.SUPERVISOR.getType() : dirType;
			Deque<AbstractEmployee> employeesByType = employeesWorking.get(priorityEmployee == 0? EmployeeType.OPERATOR.getType() : supType);
			while (employeePriority < employeesByType.size()) {
				employee = employeesByType.poll();
				employeesByType.offer(employee);
				// if available, removes head and points to last
				if (employee.getStatus().equals(EmployeeStatus.AVAILABLE.getStatus())) {
					flag = true;
					break;
				} else {
					employee = null;
					employeePriority++;
				}
				
			}
			priorityEmployee++;
		}
		return employee;
		
	}
	
	public void addEmployeeToWork(AbstractEmployee employee) {
		employeesWorking.get(employee.getTypeEmployee()).add(employee);
		
	}
	
}
