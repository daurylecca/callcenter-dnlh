package co.com.almundo.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import co.com.almundo.domain.AbstractEmployee;
import co.com.almundo.domain.Call;
import co.com.almundo.domain.DirectorSupport;
import co.com.almundo.domain.OperatorSupport;
import co.com.almundo.domain.SupervisorSupport;
import co.com.almundo.utils.EmployeeType;


public class Dispatcher{
	
	private static final int MAX_NUMBER_CALLS = 10;
	private ExecutorService executor;
	private EmployeeController employeeController;
	
	/** Logger constant. */
	private static final Logger logger = Logger.getLogger(Dispatcher.class);
	
		
	public Dispatcher(EmployeeController employeeControler) {
		this.executor = Executors.newFixedThreadPool(MAX_NUMBER_CALLS);
		this.employeeController = employeeControler;
		
	}
	
	public void dispatchCall(long clientCallId, String description) {
		AbstractEmployee employee = employeeController.getFreeEmployee();
		if (employee != null) {
			Runnable call = new Call(clientCallId, description, employee);
			executor.execute(call);
		}else {
			return;		
		}
	}
	
		
	public void terminateDispatch() throws InterruptedException {
		executor.shutdown();
		executor.awaitTermination(MAX_NUMBER_CALLS + 1L, TimeUnit.SECONDS);
		logger.info("All threads finished.");
	}
	
	
	public static void main(String[] args) {
		try {
			
			AbstractEmployee ope1 = new OperatorSupport();
			ope1.setName("OPERADOR 1");
			ope1.setTypeEmployee(EmployeeType.OPERATOR.getType());
			AbstractEmployee ope2 = new OperatorSupport();
			ope2.setName("OPERADOR 2");
			ope2.setTypeEmployee(EmployeeType.OPERATOR.getType());
			AbstractEmployee sup1 = new SupervisorSupport();
			sup1.setName("SUPERVISOR 1");
			sup1.setTypeEmployee(EmployeeType.SUPERVISOR.getType());
			AbstractEmployee dir1 = new DirectorSupport();
			dir1.setName("DIRECTOR 1");
			dir1.setTypeEmployee(EmployeeType.DIRECTOR.getType());

			// set the chain of responsibility
			ope1.setSuccessor(sup1);
			sup1.setSuccessor(dir1);
			
			EmployeeController empCtrl = new EmployeeController();
			empCtrl.addEmployeeToWork(ope1);
			empCtrl.addEmployeeToWork(ope2);
			empCtrl.addEmployeeToWork(sup1);
			empCtrl.addEmployeeToWork(dir1);
			
			Dispatcher dispatcher = new Dispatcher(empCtrl);
			dispatcher.dispatchCall(1, "Cliente 1");
			dispatcher.dispatchCall(2, "Cliente 2");
			dispatcher.dispatchCall(3, "Cliente 3");
			dispatcher.dispatchCall(4, "Cliente 4");
			dispatcher.dispatchCall(5, "Cliente 5");
			dispatcher.dispatchCall(6, "Cliente 6");
			dispatcher.dispatchCall(7, "Cliente 7");
			dispatcher.dispatchCall(8, "Cliente 8");
			dispatcher.dispatchCall(9, "Cliente 9");
			
			
			dispatcher.terminateDispatch();
		} catch (InterruptedException e) {
			logger.error("ERROR "+ e);
			Thread.currentThread().interrupt();

		}
		
		
	}
}
