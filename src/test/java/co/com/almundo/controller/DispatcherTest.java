package co.com.almundo.controller;
import org.junit.Before;
import org.junit.Test;

import co.com.almundo.controller.Dispatcher;
import co.com.almundo.controller.EmployeeController;
import co.com.almundo.domain.AbstractEmployee;
import co.com.almundo.domain.OperatorSupport;
import co.com.almundo.utils.EmployeeType;




public class DispatcherTest {
	
	Dispatcher dis;
	Dispatcher dis2;	
	Dispatcher dis3;
	
	@Before
	public void setUp() throws Exception {
		
		AbstractEmployee ope1 = new OperatorSupport();
		ope1.setName("OPERADOR 1");
		ope1.setTypeEmployee(EmployeeType.OPERATOR.getType());
		
		AbstractEmployee ope2 = new OperatorSupport();
		ope2.setName("OPERADOR 2");
		ope2.setTypeEmployee(EmployeeType.OPERATOR.getType());
		
		AbstractEmployee ope3 = new OperatorSupport();
		ope3.setName("OPERADOR 3");
		ope3.setTypeEmployee(EmployeeType.OPERATOR.getType());
		
		AbstractEmployee ope4 = new OperatorSupport();
		ope4.setName("OPERADOR 4");
		ope4.setTypeEmployee(EmployeeType.OPERATOR.getType());
		
		AbstractEmployee ope5 = new OperatorSupport();
		ope5.setName("OPERADOR 5");
		ope5.setTypeEmployee(EmployeeType.OPERATOR.getType());
		
		AbstractEmployee ope6 = new OperatorSupport();
		ope6.setName("OPERADOR 6");
		ope6.setTypeEmployee(EmployeeType.OPERATOR.getType());
		
		AbstractEmployee sup1 = new OperatorSupport();
		sup1.setName("SUPERVISOR 1");
		sup1.setTypeEmployee(EmployeeType.SUPERVISOR.getType());
		
		AbstractEmployee sup2 = new OperatorSupport();
		sup2.setName("SUPERVISOR 2");
		sup2.setTypeEmployee(EmployeeType.SUPERVISOR.getType());
		
		AbstractEmployee sup3 = new OperatorSupport();
		sup3.setName("SUPERVISOR 3");
		sup3.setTypeEmployee(EmployeeType.SUPERVISOR.getType());
		
		AbstractEmployee dir1 = new OperatorSupport();
		dir1.setName("DIRECTOR 1");
		dir1.setTypeEmployee(EmployeeType.DIRECTOR.getType());
		
		EmployeeController empCtrl = new EmployeeController();
		empCtrl.addEmployeeToWork(ope1);
		dis = new Dispatcher(empCtrl);
		
		EmployeeController empCtrl2 = new EmployeeController();
		empCtrl2.addEmployeeToWork(ope1);
		empCtrl2.addEmployeeToWork(ope2);
		empCtrl2.addEmployeeToWork(ope3);
		empCtrl2.addEmployeeToWork(ope4);
		empCtrl2.addEmployeeToWork(ope5);
		empCtrl2.addEmployeeToWork(ope6);
		empCtrl2.addEmployeeToWork(sup1);
		empCtrl2.addEmployeeToWork(sup2);
		empCtrl2.addEmployeeToWork(sup3);
		empCtrl2.addEmployeeToWork(dir1);
		dis2 = new Dispatcher(empCtrl2);
		
		EmployeeController empCtrl3 = new EmployeeController();
		empCtrl3.addEmployeeToWork(ope1);
		empCtrl3.addEmployeeToWork(sup1);
		empCtrl3.addEmployeeToWork(dir1);
		dis3 = new Dispatcher(empCtrl3);
		
	}
	
	@Test
	public void runDispatcher() throws InterruptedException {
		dis.dispatchCall(1, "Cliente 1");
		dis.terminateDispatch();
	}
	
	@Test
	public void runDispatcherException() throws InterruptedException {
		EmployeeController empCtrl = new EmployeeController();
		dis = new Dispatcher(empCtrl);
		dis.dispatchCall(1, "Cliente 1");
		dis.terminateDispatch();
	}
	
	@Test
	public void run10Dispatcher() throws InterruptedException {
		dis2.dispatchCall(1, "Cliente 1");
		dis2.dispatchCall(2, "Cliente 2");
		dis2.dispatchCall(3, "Cliente 3");
		dis2.dispatchCall(4, "Cliente 4");
		dis2.dispatchCall(5, "Cliente 5");
		dis2.dispatchCall(6, "Cliente 6");
		dis2.dispatchCall(7, "Cliente 7");
		dis2.dispatchCall(8, "Cliente 8");
		dis2.dispatchCall(9, "Cliente 9");
		dis2.dispatchCall(10, "Cliente 10");
		dis2.terminateDispatch();
	}
	
	@Test
	public void run11Dispatcher() throws InterruptedException {
		dis2.dispatchCall(1, "Cliente 1");
		dis2.dispatchCall(2, "Cliente 2");
		dis2.dispatchCall(3, "Cliente 3");
		dis2.dispatchCall(4, "Cliente 4");
		dis2.dispatchCall(5, "Cliente 5");
		dis2.dispatchCall(6, "Cliente 6");
		dis2.dispatchCall(7, "Cliente 7");
		dis2.dispatchCall(8, "Cliente 8");
		dis2.dispatchCall(9, "Cliente 9");
		dis2.dispatchCall(10, "Cliente 10");
		dis2.dispatchCall(11, "Cliente 11");
		dis2.terminateDispatch();
	}
	
	@Test
	public void runEmployeeBusy() throws InterruptedException {
		dis3.dispatchCall(1, "Cliente 1");
		dis3.dispatchCall(2, "Cliente 2");
		dis3.dispatchCall(3, "Cliente 3");
		dis3.dispatchCall(4, "Cliente 4");
	}
	
}
