package co.com.almundo.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CallTest {
	Call call;
	
	@Before
	public void setUp() throws Exception {
		AbstractEmployee emp = new OperatorSupport();
		call = new Call(1, "Cliente 1", emp);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void durationCall() throws InterruptedException {
		assertTrue(call.getDuration()/1000 >= call.MIN_DURATION);
		assertTrue(call.getDuration()/1000 <= call.MAX_DURATION);
		
	}
	
	@Test
	public void runCall() {
		call.run();
	}
	
}
